package com.tsystems.javaschool.tasks.calculator;


import java.text.DecimalFormat;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (statement==null||statement.equals("")) return null;
        if (isParenthesesClosed(statement)) {
            DecimalFormat df = new DecimalFormat("#.#####");
            StringBuilder statement1 = new StringBuilder(statement);
            StringBuilder s1 = new StringBuilder();
            StringBuilder s2 = new StringBuilder();
            char e;
            if (statement.contains("++") || statement.contains("**") ||
                    statement.contains("//") || statement.contains("--") || statement.startsWith(")") ||
                    statement.contains("..")||statement.contains(",")) {
                return null;
            }

            //This if statements calculates the value in parentheses(Only 1 pair of parentheses is allowed)
            if (statement.contains("(")){
                String parExp;
                StringBuilder s3 = new StringBuilder();
                StringBuilder s4 = new StringBuilder();
                char r;
                parExp = statement.substring(statement1.indexOf("(")+1, statement1.indexOf(")"));
                int k=0;
                do {
                    s4.append(parExp.charAt(k));
                    k++;
                }while(Character.isDigit(parExp.charAt(k)));
                r=parExp.charAt(k);
                k++;
                for (int i = k; i < parExp.length(); i++) {

                    if (!Character.isDigit(parExp.charAt(i))) {
                        String temp = calculate(s4.toString(),s3.toString(),r).toString();
                        r = parExp.charAt(i);
                        s4.replace(0, s4.length(), temp);
                        s3.delete(0, s3.length());
                        continue;
                    }
                    s3.append(parExp.charAt(i));
                }
                s4.replace(0, s4.length(), calculate(s4.toString(),s3.toString(),r).toString());
                statement1.replace(statement.indexOf("("), statement.indexOf(")")+1, s4.toString());
            }
            //Here calculates numbers with "*", "/" signs and throws an exception if infinity value results after division
            try {
                if (statement1.toString().contains("/")) statement1 = calculatingMultDiv(statement1.toString(), '/');
                if (statement1.toString().contains("*")) statement1 = calculatingMultDiv(statement1.toString(), '*');
            }catch (ArithmeticException e1){
                return null;
            }
            //If expression is solved this block return the answer
            if ((!statement1.toString().contains("+")&&
                    !statement1.toString().contains("-")) ||
                    (statement1.charAt(0)=='-'
                    ||!statement1.toString().contains("+")
                    &&!statement1.toString().contains("-"))){
                return df.format(Double.parseDouble(statement1.toString())).replace(',', '.');
            }

            //Performs other operations such as "+" or "-" and return an answer
            if (Character.isDigit(statement1.charAt(0))) {
                int o = 0;
                do {
                    s2.append(statement1.charAt(o));
                    o++;
                } while (Character.isDigit(statement1.charAt(o))||statement1.charAt(o)=='.');
                e = statement1.charAt(o);
                o++;

                for (int i = o; i < statement1.length(); i++) {
                    if (!Character.isDigit(statement1.charAt(i))
                            && statement1.charAt(i)!='-' && statement1.charAt(i)!='.') {
                        String temp = calculate(s2.toString(), s1.toString(), e).toString();
                        e = statement1.charAt(i);
                        s2.replace(0, s2.length(), temp);
                        s1.delete(0, s1.length());
                        continue;
                    }
                    s1.append(statement1.charAt(i));
                }
                s2.replace(0, s2.length(), calculate(s2.toString(), s1.toString(), e).toString());
                return df.format(Double.parseDouble(s2.toString())).replace(',', '.');
            }

            }
        return null;
    }

    private StringBuilder calculatingMultDiv(String statement, Character symbol) throws ArithmeticException{
                StringBuilder newStatement = new StringBuilder(statement);
                StringBuilder s1 = new StringBuilder();
                StringBuilder s2 = new StringBuilder();
                int counter1 = 1, counter2 = 1;
                int signInd, leftInd, rightInd;

                    signInd = statement.indexOf(symbol.toString());
                    do {
                        s2.append(statement.charAt(signInd - counter1));
                        leftInd = signInd - counter1;
                        counter1++;
                    } while ((signInd - counter1 >= 0 && Character.isDigit(statement.charAt(signInd - counter1)))
                            || (signInd - counter1 >= 0 && statement.charAt(signInd - counter1) == '.'));

                    do {
                        s1.append(statement.charAt(signInd + counter2));
                        rightInd = statement.indexOf(symbol.toString()) + counter2;
                        counter2++;
                    } while ((statement.length() > counter2 + signInd && Character.isDigit(statement.charAt(signInd + counter2)))
                            || (statement.length() > counter2 + signInd && statement.charAt(signInd + counter2) == '.'));
                    s2.reverse();
                    Double s3 = calculate(s2.toString(), s1.toString(), symbol);
                    if (Double.isInfinite(s3)) throw new ArithmeticException();
                    newStatement.replace(leftInd, rightInd+1, s3.toString());
                    return newStatement;
    }

    private Double calculate(String s1, String s2, char symbol) {
        int sign=1;
        switch (symbol) {
            case '-':
                return Double.parseDouble(s1) - Double.parseDouble(s2);
            case '+':
                return Double.parseDouble(s1) + Double.parseDouble(s2);
            case '/': {
                if (s1.charAt(0)=='-'^s1.charAt(0)=='-') sign*=-1;
                return Double.parseDouble(s1) / Double.parseDouble(s2)*sign;
            }
            case '*': {
                if (s1.charAt(0)=='-'^s2.charAt(0)=='-') sign*=-1;
                return Double.parseDouble(s1) * Double.parseDouble(s2)*sign;
            }
        }
        return null;
    }

    private boolean isParenthesesClosed(String s){
        char[] c = s.toCharArray();
        int count=0;
        for (int i=0; i<c.length; i++){
            if (c[i]=='(') count++;
            if (c[i]==')') count--;
        }

        if (count==0) return true;
        else {
            return false;
        }
    }

}
