package com.tsystems.javaschool.tasks.pyramid;



public class PyramidParams{
    private int height, width;

    public PyramidParams(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
