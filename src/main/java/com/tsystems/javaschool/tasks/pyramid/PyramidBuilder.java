package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (inputNumbers==null||inputNumbers.isEmpty()||inputNumbers.contains(null)) throw new CannotBuildPyramidException();

         //i was getting outofmemoryerror, so i decided to add try statement to solve this problem

        try {
            inputNumbers.sort(Comparator.naturalOrder());
        }catch (Error e){
            throw new CannotBuildPyramidException();
        }
        int height=getParams(inputNumbers).getHeight(), width=getParams(inputNumbers).getWidth();
        int[][] pyramid = new int[height][width];
        int c=0;

        Set<Integer> list = new HashSet<>(); //define the indexes of elements we need to write
        List<Integer> copiedList = new ArrayList<>();
        list.add(width/2);

        for (int i = 0; i < height; i++) {
            copiedList.addAll(list);
            list.clear();
            for (Integer n :
                    copiedList) {
                pyramid[i][n]=
                        inputNumbers.get(c);
                list.add(n-1);
                list.add(n+1);
                c++;
            }
            copiedList.clear();
        }
        return pyramid;
    }

    private PyramidParams getParams(List<Integer> inputNumbers){
        int numberOfElements=1, height=0, size=inputNumbers.size();

        while (size-numberOfElements>=0){
            size-=numberOfElements;
            height++;
            if (size==0) break;
            numberOfElements++;

        }

        if (size==0){
            return new PyramidParams(height, numberOfElements+numberOfElements-1);
        }else {
            throw new CannotBuildPyramidException();
        }
    }

}

